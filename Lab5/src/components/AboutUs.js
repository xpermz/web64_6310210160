import  Paper  from '@mui/material/Paper';
import { width } from '@mui/system';
import  Box from '@mui/system/Box';

function AboutUs(props){


    return(
        <div align ="center">

        <Box sx = { {width : "60%"} } >
        <Paper elevation ={3}>
            <h2> จัดทำโดย {props.name}</h2>
            <h3> ติดต่อThanaได้ที่ {props.address}</h3>
            <h3> บ้านอยู่ที่ {props.province}</h3>
        </Paper>
        </Box>

        </div>
    );



}

export default AboutUs;