import logo from './logo.svg';
import './App.css';

import AboutUsPage from './pages/AboutUsPage';
import Header from './components/Header';
import BMICalPage from './pages/BMICalPage';
import { Routes, Route } from "react-router-dom";

function App() {
  return (
    <div className="App">

      <Header/>
      <Routes>
        <Route path="about" element={
          <AboutUsPage />
        } />
        <Route path="/" element={
          <BMICalPage />
        } />

      </Routes>
    </div>
  );
}

export default App;
