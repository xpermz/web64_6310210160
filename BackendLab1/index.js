const express = require('express')
const app = express()
const port = 4000

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.get('/hello', (req, res) => {
    res.send('Sawasdee '+ req.query.name)
  })

app.post('/bmi', (req, res) => {
    let weight = parseFloat(req.query.weight)
    let height = parseFloat(req.query.height)

    var result = {}
    if( !isNaN(weight) && !isNaN(height)){
            let bmi = weight /(height * height)
             result = {
                "status" :200,
                "bmi" : bmi
            }
    }else{
        result ={
            "status" : 400,
            "massage" : "Weight or Height is not a number"
        }
    }
    res.send(JSON.stringify(result))
})

app.get('/triangle', (req, res) => {
    let height = parseFloat(req.query.height)
    let base = parseFloat(req.query.base)
    var result = {}

    if( !isNaN(base) && !isNaN(height)){
            let area = (1/2)*base*height
             result = {
                "status" :200,
                "area" : area
            }
    }else{
        result ={
            "status" : 400,
            "massage" : "base or Height is not a number"
        }
    }
    res.send(JSON.stringify(result))

    
  })

  app.post('/score', (req, res) => {
    let score = parseInt(req.query.score)
    var result = {}
    var grade = {}
    const name = {}
    if( score <100 && score>0){
             if(score >=80 ){
                 grade = "A"
             }else if(score >=70){
                 grade = "B"
             }else if(score >=60){
                 grade = "C"
             }else if(score >= 50){
                 grade ="D"
             }else{
                 grade = "F"
             }

        result = {
            "status" : 200,
            "name" : req.query.name,
            "grade" : grade
            
        }
    }else{
        result ={
            "status" : 400,
            "massage" : "Score is not in the scope"
        }
    }
    res.send(JSON.stringify(result))

    
  })


app.listen(port, () => {
  console.log(`Example app listening on port ${port}`)
})

